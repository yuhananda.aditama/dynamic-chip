package id.bootcamp.dynamicchip

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import id.bootcamp.dynamicchip.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        for (i in 0..10) {
            val chip = Chip(this)
            chip.text = "Chip $i"
            //Wajib dikasih jika ingin terdeteksi di setOnCheckedStateChangeListener
            chip.isCheckable = true
            binding.chipGroup.addView(chip)
        }

        binding.chipGroup.setOnCheckedStateChangeListener { group, checkedIds ->
            for (id in checkedIds){
                val chip: Chip = group.findViewById(id)

                Toast.makeText(
                    applicationContext,
                    "Chip is " + chip.text.toString(),
                    Toast.LENGTH_SHORT
                ).show()

                //jika tidak ingin ada tanda kalau dia di check
                chip.isChecked = false
            }
        }
    }
}